﻿using System.Text;

namespace HakunaPatata
{

    internal class Program
    {
        static void Main(string[] args)
        {
            string[] arrProductNaam = new string[] { "" }, arrKlantNaam = new string[] { "" }, arrTempString1, arrCategorieProducten = new string[] { "" }, arrHoudbaarheidsdatum = new string[] { "" }, arrTempString2, arrTempString3, arrTelefoonnummer = new string[] { "" }, arrTempLaden, arrTempLaden2;
            double[] arrVerkoopprijsProducten = new double[] { 0 }, arrTempDouble1, arrTempDouble2, arrAankoopprijsProducten = new double[] { 0 }, arrTotaalSaldo = new double[] { 0 };
            int[] arrAantalStock = new int[] { 0 }, arrTempInt1, arrTempInt2, arrMinimumVoorraadNiveau = new int[] { 0 }, arrSchijvenVan10 = new int[] { 0 };

            string nieuwProductNaam, nieuwCategorie, nieuwVerkoopprijs, nieuwAankoopprijs, nieuwAantalStock, nieuwMinimumVoorraadNiveau, nieuwHoudbaarheidsdatum, nieuwKlantNaam, nieuwTelefoonnummer, nieuwTotaalSaldo, nieuwSchijvenVan10, invoerString, alleProducten = "", alleKlanten = "", alleProducten1 = "", alleKlanten1 = "", bestelling = "";
            bool checkProduct = false, exit = false, exit1, exit3, exit4, exit5, exit6, checkKlant = false, exit7 = false, exit8, exit9;
            int tellerProduct, invoer, tellerKlant, index = -1, stockFriet = 0, tellerLaden, j, schijfVan10;
            double individueleRekening, omzetVandaag = 0, aankoopprijsVandaag = 0;
            DateTime datum;

            if (System.IO.File.Exists("productgegevens.txt"))
            {
                StreamReader leesobject = new StreamReader("productgegevens.txt");
                tellerLaden = -1;
                while (!leesobject.EndOfStream)
                {
                    tellerLaden++;
                    alleProducten += leesobject.ReadLine();
                }
                arrTempLaden = new string[tellerLaden * 7];
                arrTempLaden = alleProducten.Split(";");
                j = 0;

                for (int i = 0; i < tellerLaden; i++)
                {
                    Array.Resize(ref arrProductNaam, i + 1);
                    arrProductNaam[i] = arrTempLaden[j];
                    Array.Resize(ref arrCategorieProducten, i + 1);
                    arrCategorieProducten[i] = arrTempLaden[j + 1];
                    Array.Resize(ref arrVerkoopprijsProducten, i + 1);
                    arrVerkoopprijsProducten[i] = Convert.ToDouble(arrTempLaden[j + 2]);
                    Array.Resize(ref arrAankoopprijsProducten, i + 1);
                    arrAankoopprijsProducten[i] = Convert.ToDouble(arrTempLaden[j + 3]);
                    Array.Resize(ref arrAantalStock, i + 1);
                    arrAantalStock[i] = Convert.ToInt32(arrTempLaden[j + 4]);
                    Array.Resize(ref arrMinimumVoorraadNiveau, i + 1);
                    arrMinimumVoorraadNiveau[i] = Convert.ToInt32(arrTempLaden[j + 5]);
                    Array.Resize(ref arrHoudbaarheidsdatum, i + 1);
                    arrHoudbaarheidsdatum[i] = arrTempLaden[j + 6];
                    j += 7;
                }

                leesobject.Close();

                Console.WriteLine("Uw productgegevens werden geladen.");
            }

            if (System.IO.File.Exists("klantgegevens.txt"))
            {
                StreamReader leesobject2 = new StreamReader("klantgegevens.txt");
                tellerLaden = -1;
                while (!leesobject2.EndOfStream)
                {
                    tellerLaden++;
                    alleKlanten += leesobject2.ReadLine();
                }
                arrTempLaden2 = new string[tellerLaden * 4];
                arrTempLaden2 = alleKlanten.Split(";");
                j = 0;

                for (int i = 0; i < tellerLaden; i++)
                {
                    Array.Resize(ref arrKlantNaam, i + 1);
                    arrKlantNaam[i] = arrTempLaden2[j];
                    Array.Resize(ref arrTelefoonnummer, i + 1);
                    arrTelefoonnummer[i] = arrTempLaden2[j + 1];
                    Array.Resize(ref arrTotaalSaldo, i + 1);
                    arrTotaalSaldo[i] = Convert.ToDouble(arrTempLaden2[j + 2]);
                    Array.Resize(ref arrSchijvenVan10, i + 1);
                    arrSchijvenVan10[i] = Convert.ToInt32(arrTempLaden2[j + 3]);
                    j += 4;
                }


                leesobject2.Close();

                Console.WriteLine("Uw klantgegevens werden geladen.");
            }

            if (!System.IO.File.Exists("productgegevens.txt") && !System.IO.File.Exists("klantgegevens.txt"))
            {
                Console.WriteLine("Er werden geen gegevens gevonden. We maken ze aan.");
                using (StreamWriter writer = new StreamWriter("productgegevens.txt"))
                { }
                using (StreamWriter writer = new StreamWriter("klantgegevens.txt"))
                { Console.WriteLine("Uw gegevens werden aangemaakt."); }
            }


            while (!exit)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("=====================================================================================");
                Console.ForegroundColor = ConsoleColor.Yellow;
               
                Console.WriteLine("   _    _          _  ___    _ _   _            _____     _______    _______       ");
                Console.WriteLine("  | |  | |   /\\   | |/ / |  | | \\ | |   /\\     |  __ \\ /\\|__   __|/\\|__   __|/\\    ");
                Console.WriteLine("  | |__| |  /  \\  | ' /| |  | |  \\| |  /  \\    | |__) /  \\  | |  /  \\  | |  /  \\   ");
                Console.WriteLine("  |  __  | / /\\ \\ |  < | |  | | . ` | / /\\ \\   |  ___/ /\\ \\ | | / /\\ \\ | | / /\\ \\  ");
                Console.WriteLine("  | |  | |/ ____ \\| . \\| |__| | |\\  |/ ____ \\  | |  / ____ \\| |/ ____ \\| |/ ____ \\ ");
                Console.WriteLine("  |_|  |_|_/    \\_\\_|\\_\\_____/|_| \\_/_/    \\_\\ |_| /_/    \\_\\_/_/    \\_\\_/_/    \\_\\");
                Console.WriteLine("                                                                                     ");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("=====================================================================================");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();


                Console.WriteLine("Kies een optie:");
                Console.WriteLine("1. Producten beheren.");
                Console.WriteLine("2. Klanten beheren.");
                Console.WriteLine("3. Overzicht.");
                Console.WriteLine("4. Kassa.");
                Console.WriteLine("5. Afsluiten.");
                Console.ForegroundColor = ConsoleColor.Yellow;

                Console.OutputEncoding = Encoding.UTF8;
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⢤⣆⠀⣀⡀⠀⣤⡄⢸⣿⠀⣶⠀⣼⡇⣠⡀⢀⣷⠂⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠈⠉⠀⢻⣿⠀⢿⡇⢸⣿⠀⡿⠀⣿⠃⣿⠁⣼⡟⠀⣶⡄⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⢿⡄⠘⣿⡇⠈⡁⠸⣿⠀⠁⢰⡏⢀⡁⢰⣿⠃⣸⣿⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠈⢿⣆⠘⣿⠀⢻⡀⢻⡇⠀⢸⡇⢸⡇⢸⡟⢀⣿⡇⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠈⠻⠆⠘⠇⢸⣇⠘⡇⠀⣾⠀⣾⠃⠸⠁⠘⠋⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⢺⣶⣦⣤⣀⣀⣉⣀⣁⣀⣉⣀⣁⣤⣤⣶⣶⡗⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠀⣿⣯⡉⠉⣭⣭⣭⣭⣭⣭⣭⣭⠉⢉⣽⣿⠀⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠀⣿⣿⣿⣆⠘⢿⣿⣿⣿⣿⡿⠃⣰⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣷⣄⠙⢿⡿⠋⣠⣾⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣦⡈⢁⣴⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠀⠈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁⠀⠀⠀⠀⠀⠀⠀");
                Console.WriteLine("⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠀⠀⠀⠀⠀⠀⠀⠀");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("Voer het nummer van de gewenste optie in: ");

                int.TryParse(Console.ReadLine(), out int keuze);
                {
                    switch (keuze)
                    {
                        case 1:
                            exit1 = false;

                            while (!exit1)
                            {
                                Console.Clear();
                                Console.WriteLine("Productenbeheer.");
                                Console.WriteLine("Kies een optie:");
                                Console.WriteLine("1. Producten toevoegen.");
                                Console.WriteLine("2. Producten wijzigen.");
                                Console.WriteLine("3. Producten verwijderen.");
                                Console.WriteLine("4. Terug naar hoofdmenu.");

                                Console.Write("Voer het nummer van de gewenste optie in: ");

                                int.TryParse(Console.ReadLine(), out int keuze1);
                                {
                                    switch (keuze1)
                                    {
                                        case 1:
                                            Console.Clear();
                                            tellerProduct = 0;
                                            checkProduct = false;

                                            Console.WriteLine("Voeg een product toe:");
                                            nieuwProductNaam = Console.ReadLine();

                                            while (arrProductNaam.Contains(nieuwProductNaam))
                                            {
                                                Console.WriteLine("Dit product bestaat al.");
                                                Console.WriteLine("Voeg een product toe:");
                                                nieuwProductNaam = Console.ReadLine();
                                            }

                                            while (checkProduct == false)
                                            {

                                                if (tellerProduct >= arrProductNaam.Length)
                                                {
                                                    arrTempString1 = new string[arrProductNaam.Length + 1];
                                                    for (int i = 0; i < arrProductNaam.Length; i++)
                                                    {
                                                        arrTempString1[i] = arrProductNaam[i];
                                                    }
                                                    arrTempString1[arrProductNaam.Length] = nieuwProductNaam;
                                                    arrProductNaam = arrTempString1;

                                                    arrTempString2 = new string[arrCategorieProducten.Length + 1];
                                                    for (int i = 0; i < arrCategorieProducten.Length; i++)
                                                    {
                                                        arrTempString2[i] = arrCategorieProducten[i];
                                                    }
                                                    VraagCategorie(nieuwProductNaam, out nieuwCategorie);
                                                    arrTempString2[arrCategorieProducten.Length] = nieuwCategorie;
                                                    arrCategorieProducten = arrTempString2;

                                                    arrTempDouble1 = new double[arrVerkoopprijsProducten.Length + 1];
                                                    for (int i = 0; i < arrVerkoopprijsProducten.Length; i++)
                                                    {
                                                        arrTempDouble1[i] = arrVerkoopprijsProducten[i];
                                                    }
                                                    VraagVerkoopprijs(nieuwProductNaam, out nieuwVerkoopprijs);
                                                    arrTempDouble1[arrVerkoopprijsProducten.Length] = Math.Round(Convert.ToDouble(nieuwVerkoopprijs), 2);
                                                    arrVerkoopprijsProducten = arrTempDouble1;

                                                    arrTempDouble2 = new double[arrVerkoopprijsProducten.Length + 1];
                                                    for (int i = 0; i < arrAankoopprijsProducten.Length; i++)
                                                    {
                                                        arrTempDouble2[i] = arrAankoopprijsProducten[i];
                                                    }
                                                    VraagAankoopprijs(nieuwProductNaam, out nieuwAankoopprijs);
                                                    arrTempDouble2[arrAankoopprijsProducten.Length] = Math.Round(Convert.ToDouble(nieuwAankoopprijs), 2);
                                                    arrAankoopprijsProducten = arrTempDouble2;

                                                    arrTempInt1 = new int[arrAantalStock.Length + 1];
                                                    for (int i = 0; i < arrAantalStock.Length; i++)
                                                    {
                                                        arrTempInt1[i] = arrAantalStock[i];
                                                    }
                                                    VraagAantalStock(nieuwProductNaam, out nieuwAantalStock);
                                                    arrTempInt1[arrAantalStock.Length] = Convert.ToInt32(nieuwAantalStock);
                                                    arrAantalStock = arrTempInt1;

                                                    arrTempInt2 = new int[arrAantalStock.Length + 1];
                                                    for (int i = 0; i < arrMinimumVoorraadNiveau.Length; i++)
                                                    {
                                                        arrTempInt2[i] = arrMinimumVoorraadNiveau[i];
                                                    }
                                                    VraagMinimumVoorraadNiveau(nieuwProductNaam, out nieuwMinimumVoorraadNiveau);
                                                    arrTempInt2[arrMinimumVoorraadNiveau.Length] = Convert.ToInt32(nieuwMinimumVoorraadNiveau);
                                                    arrMinimumVoorraadNiveau = arrTempInt2;

                                                    arrTempString3 = new string[arrHoudbaarheidsdatum.Length + 1];
                                                    for (int i = 0; i < arrHoudbaarheidsdatum.Length; i++)
                                                    {
                                                        arrTempString3[i] = arrHoudbaarheidsdatum[i];
                                                    }
                                                    VraagHoudbaarheidsdatum(nieuwProductNaam, out nieuwHoudbaarheidsdatum);
                                                    arrTempString3[arrHoudbaarheidsdatum.Length] = nieuwHoudbaarheidsdatum;
                                                    arrHoudbaarheidsdatum = arrTempString3;

                                                    checkProduct = true;
                                                }
                                                else if (arrProductNaam[tellerProduct] == null || arrProductNaam[tellerProduct] == "")
                                                {
                                                    arrProductNaam[tellerProduct] = nieuwProductNaam;

                                                    VraagCategorie(nieuwProductNaam, out nieuwCategorie);
                                                    arrCategorieProducten[tellerProduct] = nieuwCategorie;

                                                    VraagVerkoopprijs(nieuwProductNaam, out nieuwVerkoopprijs);
                                                    arrVerkoopprijsProducten[tellerProduct] = Math.Round(Convert.ToDouble(nieuwVerkoopprijs), 2);

                                                    VraagAankoopprijs(nieuwProductNaam, out nieuwAankoopprijs);
                                                    arrAankoopprijsProducten[tellerProduct] = Math.Round(Convert.ToDouble(nieuwAankoopprijs), 2);

                                                    VraagAantalStock(nieuwProductNaam, out nieuwAantalStock);
                                                    arrAantalStock[tellerProduct] = Convert.ToInt32(nieuwAantalStock);

                                                    VraagMinimumVoorraadNiveau(nieuwProductNaam, out nieuwMinimumVoorraadNiveau);
                                                    arrMinimumVoorraadNiveau[tellerProduct] = Convert.ToInt32(nieuwMinimumVoorraadNiveau);

                                                    VraagHoudbaarheidsdatum(nieuwProductNaam, out nieuwHoudbaarheidsdatum);
                                                    arrHoudbaarheidsdatum[tellerProduct] = nieuwHoudbaarheidsdatum;

                                                    checkProduct = true;
                                                }
                                                else
                                                {
                                                    tellerProduct++;
                                                }

                                            }

                                            break;

                                        case 2:
                                            Console.Clear();
                                            exit3 = false;
                                            Console.WriteLine("Productwijziging.");
                                            while (!exit3)
                                            {

                                                Console.WriteLine($"Welk product wil je bewerken? Geef {arrProductNaam.Length + 1} in om terug te gaan naar het vorige scherm.");
                                                for (int i = 0; i < arrProductNaam.Length; i++)
                                                { Console.WriteLine($"{i + 1}. {arrProductNaam[i]}"); }
                                                int.TryParse(Console.ReadLine(), out invoer);

                                                if (invoer > 0 && invoer <= arrProductNaam.Length)
                                                {
                                                    exit4 = false;
                                                    Console.WriteLine($"Wat wil je bewerken van het product {arrProductNaam[invoer - 1]}?");
                                                    Console.WriteLine("1. " + "De naam.");
                                                    Console.WriteLine("2. " + "De categorie.");
                                                    Console.WriteLine("3. " + "De verkoopprijs.");
                                                    Console.WriteLine("4. " + "De aankoopprijs.");
                                                    Console.WriteLine("5. " + "Aantal in stock.");
                                                    Console.WriteLine("6. " + "Het minimumvoorraadniveau.");
                                                    Console.WriteLine("7. " + "De houdbaarheidsdatum.");
                                                    int.TryParse(Console.ReadLine(), out int keuzeBewerking);
                                                    while (!exit4)
                                                    {
                                                        switch (keuzeBewerking)
                                                        {
                                                            case 1:
                                                                Console.WriteLine($"Geef een nieuwe naam in voor een {arrProductNaam[invoer - 1]}.");
                                                                arrProductNaam[invoer - 1] = Console.ReadLine();
                                                                exit4 = true;
                                                                break;

                                                            case 2:
                                                                Console.WriteLine($"Geef een nieuwe categorie in voor een {arrProductNaam[invoer - 1]}.");
                                                                nieuwCategorie = Console.ReadLine().ToUpper();
                                                                while (nieuwCategorie != "FRIET" && nieuwCategorie != "VLEES" && nieuwCategorie != "SAUS" && nieuwCategorie != "DRANK")
                                                                {
                                                                    Console.WriteLine("Ongeldige invoer. Voer Friet, Vlees, Saus of Drank in.");
                                                                    nieuwCategorie = Console.ReadLine().ToUpper();
                                                                }
                                                                arrCategorieProducten[invoer - 1] = nieuwCategorie;
                                                                exit4 = true;
                                                                break;
                                                            case 3:
                                                                Console.WriteLine($"Geef een nieuwe verkoopprijs in voor een {arrProductNaam[invoer - 1]}.");
                                                                nieuwVerkoopprijs = Console.ReadLine();
                                                                while (!IsGeldigePrijs(nieuwVerkoopprijs))
                                                                {
                                                                    Console.WriteLine("Dit is geen geldige prijs, gelieve een in te voeren.");
                                                                    nieuwVerkoopprijs = Console.ReadLine();

                                                                }
                                                                arrVerkoopprijsProducten[invoer - 1] = Math.Round(Convert.ToDouble(nieuwVerkoopprijs), 2);
                                                                exit4 = true;
                                                                break;
                                                            case 4:
                                                                Console.WriteLine($"Geef een nieuwe aankoopprijs per stuk in voor een {arrProductNaam[invoer - 1]}.");
                                                                nieuwAankoopprijs = Console.ReadLine();
                                                                while (!IsGeldigePrijs(nieuwAankoopprijs))
                                                                {
                                                                    Console.WriteLine("Dit is geen geldige prijs, gelieve een in te voeren.");
                                                                    nieuwAankoopprijs = Console.ReadLine();
                                                                }
                                                                if (arrCategorieProducten[invoer - 1] == "FRIET")
                                                                {
                                                                    for (int i = 0; i < arrProductNaam.Length; i++)
                                                                    {
                                                                        if (arrCategorieProducten[i] == "FRIET")
                                                                        { arrAankoopprijsProducten[i] = Math.Round(Convert.ToDouble(nieuwAankoopprijs), 2); }
                                                                    }
                                                                }
                                                                else
                                                                { arrAankoopprijsProducten[invoer - 1] = Math.Round(Convert.ToDouble(nieuwAankoopprijs), 2); }

                                                                exit4 = true;
                                                                break;
                                                            case 5:
                                                                Console.WriteLine($"Geef een het nieuwe aantal stock in voor een {arrProductNaam[invoer - 1]}.");
                                                                nieuwAantalStock = Console.ReadLine();
                                                                while (!IsGeldigGetal(nieuwAantalStock))
                                                                {
                                                                    Console.WriteLine("Dit is geen geldig aantal, gelieve een in te voeren");
                                                                    nieuwAantalStock = Console.ReadLine();
                                                                }
                                                                if (arrCategorieProducten[invoer - 1] == "FRIET")
                                                                {
                                                                    for (int i = 0; i < arrProductNaam.Length; i++)
                                                                    {
                                                                        if (arrCategorieProducten[i] == "FRIET")
                                                                        { arrAantalStock[i] = Convert.ToInt32(nieuwAantalStock); }
                                                                    }
                                                                }

                                                                else

                                                                { arrAantalStock[invoer - 1] = Convert.ToInt32(nieuwAantalStock); }
                                                                exit4 = true;
                                                                break;

                                                            case 6:
                                                                Console.WriteLine($"Geef het nieuwe minimumvoorraadniveau in voor een {arrProductNaam[invoer - 1]}.");
                                                                nieuwMinimumVoorraadNiveau = Console.ReadLine();
                                                                while (!IsGeldigGetal(nieuwMinimumVoorraadNiveau))
                                                                {
                                                                    Console.WriteLine("Dit is geen geldig aantal, gelieve een in te voeren");
                                                                    nieuwMinimumVoorraadNiveau = Console.ReadLine();
                                                                }
                                                                if (arrCategorieProducten[invoer - 1] == "FRIET")
                                                                {
                                                                    for (int i = 0; i < arrProductNaam.Length; i++)
                                                                    {
                                                                        if (arrCategorieProducten[i] == "FRIET")
                                                                        { arrMinimumVoorraadNiveau[i] = Convert.ToInt32(nieuwMinimumVoorraadNiveau); }
                                                                    }
                                                                }
                                                                else
                                                                { arrMinimumVoorraadNiveau[invoer - 1] = Convert.ToInt32(nieuwMinimumVoorraadNiveau); }

                                                                exit4 = true;
                                                                break;
                                                            case 7:
                                                                Console.WriteLine($"Geef een nieuwe houdbaarheidsdatum in voor een {arrProductNaam[invoer - 1]}.");
                                                                nieuwHoudbaarheidsdatum = Console.ReadLine();
                                                                while (!IsGeldigeDatum(nieuwHoudbaarheidsdatum))
                                                                {
                                                                    Console.WriteLine("Dit is geen geldige houdbaarheidsdatum, gelieve de datum in het volgende format in te voeren : dd/mm/yyyy");
                                                                    nieuwHoudbaarheidsdatum = Console.ReadLine();
                                                                }
                                                                if (arrCategorieProducten[invoer - 1] == "FRIET")
                                                                {
                                                                    for (int i = 0; i < arrProductNaam.Length; i++)
                                                                    {
                                                                        if (arrCategorieProducten[i] == "FRIET")
                                                                        { arrHoudbaarheidsdatum[i] = nieuwHoudbaarheidsdatum; }
                                                                    }
                                                                }
                                                                else
                                                                { arrHoudbaarheidsdatum[invoer - 1] = nieuwHoudbaarheidsdatum; }
                                                                exit4 = true;
                                                                break;
                                                            default:
                                                                Console.WriteLine("Gelieve een nummer tussen 1 en 7 in te voeren.");
                                                                int.TryParse(Console.ReadLine(), out invoer);
                                                                break;

                                                        }
                                                    }
                                                }
                                                else if (invoer == arrProductNaam.Length + 1)
                                                { exit3 = true; }
                                                else { Console.WriteLine("Gelieve een getal uit de lijst te kiezen."); }
                                            }
                                            break;
                                        case 3:
                                            Console.Clear();
                                            exit5 = false;
                                            Console.WriteLine("Verwijder een product.");
                                            Console.WriteLine($"Welk product wil je verwijderen? Geef {arrProductNaam.Length + 1} in om terug te gaan naar het vorige scherm.");
                                            for (int i = 0; i < arrProductNaam.Length; i++)
                                            { Console.WriteLine($"{i + 1}. {arrProductNaam[i]}"); }
                                            while (!exit5)
                                            {
                                                int.TryParse(Console.ReadLine(), out invoer);



                                                if (invoer > 0 && invoer <= arrProductNaam.Length)
                                                {
                                                    Array.Clear(arrProductNaam, invoer - 1, 1);
                                                    Array.Clear(arrCategorieProducten, invoer - 1, 1);
                                                    Array.Clear(arrVerkoopprijsProducten, invoer - 1, 1);
                                                    Array.Clear(arrAankoopprijsProducten, invoer - 1, 1);
                                                    Array.Clear(arrAantalStock, invoer - 1, 1);
                                                    Array.Clear(arrMinimumVoorraadNiveau, invoer - 1, 1);
                                                    Array.Clear(arrHoudbaarheidsdatum, invoer - 1, 1);
                                                    exit5 = true;
                                                }
                                                else if (invoer == arrProductNaam.Length + 1)
                                                { exit5 = true; }
                                                else { Console.WriteLine("Gelieve een getal uit de lijst te kiezen."); }
                                            }

                                            break;
                                        case 4:
                                            Console.Clear();
                                            Console.WriteLine("U gaat terug naar het hoofdmenu.");
                                            exit1 = true;
                                            break;
                                        default:
                                            Console.WriteLine("Ongeldige optie. Kies een geldig nummer.");
                                            break;
                                    }
                                }
                            }
                            break;
                        case 2:
                            bool exit2 = false;

                            while (!exit2)
                            {
                                Console.Clear();
                                Console.WriteLine("Klantenbeheer.");
                                Console.WriteLine("Kies een optie:");
                                Console.WriteLine("1. Klanten toevoegen.");
                                Console.WriteLine("2. Klanten wijzigen.");
                                Console.WriteLine("3. Klanten verwijderen.");
                                Console.WriteLine("4. Terug naar hoofdmenu.");

                                Console.Write("Voer het nummer van de gewenste optie in: ");

                                int.TryParse(Console.ReadLine(), out int keuze2);
                                {
                                    switch (keuze2)
                                    {
                                        case 1:
                                            Console.Clear();
                                            tellerKlant = 0;
                                            checkKlant = false;

                                            Console.WriteLine("Voeg een klant toe:");
                                            nieuwKlantNaam = Console.ReadLine();

                                            while (arrKlantNaam.Contains(nieuwKlantNaam))
                                            {
                                                Console.WriteLine("Deze klant bestaat al.");
                                                Console.WriteLine("Voeg een klant toe:");
                                                nieuwKlantNaam = Console.ReadLine();
                                            }
                                            while (checkKlant == false)
                                            {

                                                if (tellerKlant >= arrKlantNaam.Length)
                                                {
                                                    arrTempString1 = new string[arrKlantNaam.Length + 1];
                                                    for (int i = 0; i < arrKlantNaam.Length; i++)
                                                    {
                                                        arrTempString1[i] = arrKlantNaam[i];
                                                    }
                                                    arrTempString1[arrKlantNaam.Length] = nieuwKlantNaam;
                                                    arrKlantNaam = arrTempString1;

                                                    arrTempString2 = new string[arrTelefoonnummer.Length + 1];
                                                    for (int i = 0; i < arrTelefoonnummer.Length; i++)
                                                    {
                                                        arrTempString2[i] = arrTelefoonnummer[i];
                                                    }
                                                    VraagTelefoonnummer(nieuwKlantNaam, out nieuwTelefoonnummer);
                                                    arrTempString2[arrTelefoonnummer.Length] = nieuwTelefoonnummer;
                                                    arrTelefoonnummer = arrTempString2;

                                                    arrTempDouble1 = new double[arrTotaalSaldo.Length + 1];
                                                    for (int i = 0; i < arrTotaalSaldo.Length; i++)
                                                    {
                                                        arrTempDouble1[i] = arrTotaalSaldo[i];
                                                    }
                                                    VraagTotaalSaldo(nieuwKlantNaam, out nieuwTotaalSaldo);
                                                    arrTempDouble1[arrTotaalSaldo.Length] = Math.Round(Convert.ToDouble(nieuwTotaalSaldo), 2);
                                                    arrTotaalSaldo = arrTempDouble1;



                                                    arrTempInt1 = new int[arrSchijvenVan10.Length + 1];
                                                    for (int i = 0; i < arrSchijvenVan10.Length; i++)
                                                    {
                                                        arrTempInt1[i] = arrSchijvenVan10[i];
                                                    }
                                                    VraagAantalSchijven(nieuwKlantNaam, out nieuwSchijvenVan10);
                                                    arrTempInt1[arrSchijvenVan10.Length] = Convert.ToInt32(nieuwSchijvenVan10);
                                                    arrSchijvenVan10 = arrTempInt1;

                                                    checkKlant = true;
                                                }
                                                else if (arrKlantNaam[tellerKlant] == null || arrKlantNaam[tellerKlant] == "")
                                                {
                                                    arrKlantNaam[tellerKlant] = nieuwKlantNaam;

                                                    VraagTelefoonnummer(nieuwKlantNaam, out nieuwTelefoonnummer);
                                                    arrTelefoonnummer[tellerKlant] = nieuwTelefoonnummer;

                                                    VraagTotaalSaldo(nieuwKlantNaam, out nieuwTotaalSaldo);
                                                    arrTotaalSaldo[tellerKlant] = Math.Round(Convert.ToDouble(nieuwTotaalSaldo), 2);



                                                    VraagAantalSchijven(nieuwKlantNaam, out nieuwSchijvenVan10);
                                                    arrSchijvenVan10[tellerKlant] = Convert.ToInt32(nieuwSchijvenVan10);



                                                    checkKlant = true;
                                                }
                                                else
                                                {
                                                    tellerKlant++;
                                                }



                                            }
                                            break;

                                        case 2:
                                            Console.Clear();
                                            exit3 = false;
                                            Console.WriteLine("Klantwijziging.");
                                            while (!exit3)
                                            {

                                                Console.WriteLine($"Welke klant wil je bewerken? Geef {arrKlantNaam.Length + 1} in om terug te gaan naar het vorige scherm.");
                                                for (int i = 0; i < arrKlantNaam.Length; i++)
                                                { Console.WriteLine($"{i + 1}. {arrKlantNaam[i]}"); }
                                                int.TryParse(Console.ReadLine(), out invoer);

                                                if (invoer > 0 && invoer <= arrKlantNaam.Length)
                                                {
                                                    exit4 = false;
                                                    Console.WriteLine($"Wat wil je bewerken van de klant {arrKlantNaam[invoer - 1]}?");
                                                    Console.WriteLine("1. " + "De naam.");
                                                    Console.WriteLine("2. " + "Het telefoonnummer.");
                                                    Console.WriteLine("3. " + "Het totale saldo.");
                                                    Console.WriteLine("4. " + "Het aantal stempels op de klantenkaart.");

                                                    int.TryParse(Console.ReadLine(), out int keuzeBewerking);

                                                    while (!exit4)
                                                    {
                                                        switch (keuzeBewerking)
                                                        {
                                                            case 1:
                                                                Console.WriteLine($"Geef een nieuwe naam in voor {arrKlantNaam[invoer - 1]}.");
                                                                arrKlantNaam[invoer - 1] = Console.ReadLine();
                                                                exit4 = true;
                                                                break;

                                                            case 2:
                                                                Console.WriteLine($"Geef een nieuw telefoonnummer in voor {arrKlantNaam[invoer - 1]}.");
                                                                nieuwTelefoonnummer = Console.ReadLine();
                                                                while (nieuwTelefoonnummer.Length != 10 && nieuwTelefoonnummer.All(char.IsDigit))
                                                                {
                                                                    Console.WriteLine("Ongeldige invoer. Een telefoonnummer telt 10 cijfers. Probeer opnieuw");
                                                                    nieuwTelefoonnummer = Console.ReadLine();
                                                                }
                                                                arrTelefoonnummer[invoer - 1] = nieuwTelefoonnummer;
                                                                exit4 = true;
                                                                break;
                                                            case 3:
                                                                Console.WriteLine($"Geef een nieuwe verkoopprijs in voor een {arrKlantNaam[invoer - 1]}.");
                                                                nieuwTotaalSaldo = Console.ReadLine();
                                                                while (!IsGeldigePrijs(nieuwTotaalSaldo))
                                                                {
                                                                    Console.WriteLine("Dit is geen geldige prijs, gelieve een in te voeren.");
                                                                    nieuwTotaalSaldo = Console.ReadLine();
                                                                }
                                                                arrTotaalSaldo[invoer - 1] = Math.Round(Convert.ToDouble(nieuwTotaalSaldo), 2);
                                                                exit4 = true;
                                                                break;
                                                            case 4:
                                                                Console.WriteLine($"Geef een nieuwe aankoopprijs per stuk in voor een {arrKlantNaam[invoer - 1]}.");
                                                                nieuwSchijvenVan10 = Console.ReadLine();
                                                                while (!IsGeldigGetal(nieuwSchijvenVan10))
                                                                {
                                                                    Console.WriteLine("Dit is geen geldige prijs, gelieve een in te voeren.");
                                                                    nieuwSchijvenVan10 = Console.ReadLine();
                                                                }
                                                                arrSchijvenVan10[invoer - 1] = Convert.ToInt32(nieuwSchijvenVan10);

                                                                exit4 = true;
                                                                break;
                                                            default:
                                                                Console.WriteLine("Ongeldige optie. Kies een getal tussen 1 en 4.");
                                                                break;
                                                        }

                                                    }
                                                }
                                                else if (invoer == arrKlantNaam.Length + 1)
                                                { exit3 = true; }

                                                else
                                                {
                                                    Console.WriteLine("Gelieve een getal uit de lijst te kiezen.");
                                                }
                                            }
                                            break;

                                        case 3:
                                            Console.Clear();
                                            exit5 = false;
                                            Console.WriteLine($"Welke klant wil je verwijderen? typ '1' in om terug te gaan naar het vorige scherm.");
                                            for (int i = 0; i < arrKlantNaam.Length; i++)
                                            { Console.WriteLine($"{i + 1}. {arrKlantNaam[i]}"); }
                                            

                                            while (!exit5)
                                            {

                                                invoerString = Console.ReadLine();


                                                if (arrKlantNaam.Contains(invoerString, StringComparer.OrdinalIgnoreCase))
                                                {
                                                    index = Array.IndexOf(arrKlantNaam, invoerString);
                                                    while (index == -1)
                                                    {
                                                        Console.WriteLine("Gelieve rekening te houden met de juiste hoofdletters. Probeer nog een keer.");
                                                        invoerString = Console.ReadLine();
                                                        index = Array.IndexOf(arrKlantNaam, invoerString);
                                                    }
                                                    Array.Clear(arrKlantNaam, index, 1);
                                                    Array.Clear(arrTelefoonnummer, index, 1);
                                                    Array.Clear(arrTotaalSaldo, index, 1);
                                                    Array.Clear(arrSchijvenVan10, index, 1);

                                                    exit5 = true;
                                                }
                                                else if (invoerString == "1")
                                                { exit5 = true; }
                                                else { Console.WriteLine("Klant kan niet gevonden worden. Probeer nog een keer of druk op 1 om terug te gaan naar het vorige scherm."); }
                                            }

                                            break;
                                        case 4:
                                            Console.Clear();
                                            Console.WriteLine("U gaat terug naar het hoofdmenu.");
                                            exit2 = true;
                                            break;
                                        default:
                                            Console.WriteLine("Ongeldige optie. Kies een geldig nummer.");
                                            break;

                                    }
                                }


                            }
                            break;

                        case 3:
                            Console.Clear();
                            exit6 = false;
                            Console.WriteLine("Overzicht.");
                            Console.WriteLine("1. Producten.");
                            Console.WriteLine("2. Klanten.");

                            Console.Write("Voer het nummer van de gewenste optie in: ");
                            Console.WriteLine();

                            while (!exit6)
                            {
                                int.TryParse(Console.ReadLine(), out int keuze3);
                                Console.WriteLine();
                                switch (keuze3)
                                {
                                    case 1:
                                        Console.Clear();
                                        Console.WriteLine($"{"Product".PadLeft(19)} {"             Categorie".PadRight(20)} {"Verkoopprijs".PadLeft(23)} {"Aankoopprijs".PadLeft(20)} {"Aantal in stock".PadLeft(23)} {"MinVoorraadNiveau".PadLeft(22)} {"Houdbaarheidsdatum".PadLeft(21)}");
                                        Console.WriteLine();
                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                        {
                                            if (arrProductNaam[i] != null)
                                            {
                                                if (i < 9)
                                                { Console.WriteLine($"{(i + 1)}{".".PadRight(10)} {arrProductNaam[i].PadRight(20)} {arrCategorieProducten[i].PadRight(20)} {arrVerkoopprijsProducten[i].ToString().PadRight(20)} {arrAankoopprijsProducten[i].ToString().PadRight(20)} {arrAantalStock[i].ToString().PadRight(20)} {arrMinimumVoorraadNiveau[i].ToString().PadRight(20)} {arrHoudbaarheidsdatum[i].ToString().PadRight(20)}"); }
                                                else
                                                { Console.WriteLine($"{(i + 1)}{".".PadRight(9)} {arrProductNaam[i].PadRight(20)} {arrCategorieProducten[i].PadRight(20)} {arrVerkoopprijsProducten[i].ToString().PadRight(20)} {arrAankoopprijsProducten[i].ToString().PadRight(20)} {arrAantalStock[i].ToString().PadRight(20)} {arrMinimumVoorraadNiveau[i].ToString().PadRight(20)} {arrHoudbaarheidsdatum[i].ToString().PadRight(20)}"); }
                                            }
                                        }
                                        Console.WriteLine();
                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                        {
                                            if (arrAantalStock[i] <= arrMinimumVoorraadNiveau[i] && (arrCategorieProducten[i] == "VLEES" || arrCategorieProducten[i] == "DRANK"))
                                            { Console.WriteLine($"Gelieve de stock van {arrProductNaam[i]} aan te vullen. Er zijn er momenteel nog {arrAantalStock[i]}."); }
                                            else if (arrAantalStock[i] <= arrMinimumVoorraadNiveau[i] && arrCategorieProducten[i] == "SAUS")
                                            { Console.WriteLine($"Gelieve de stock van {arrProductNaam[i]} aan te vullen. Er is momenteel nog {arrAantalStock[i]}ml in stock"); }
                                            else if (arrAantalStock[i] <= arrMinimumVoorraadNiveau[i] && arrCategorieProducten[i] == "FRIET")
                                            { stockFriet += arrAantalStock[i]; }
                                        }
                                        Console.WriteLine();
                                        Console.WriteLine($"Er zou nog {(double)arrAantalStock[3] / 1000}kg friet aanwezig zijn.");

                                        Console.WriteLine();
                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                        {
                                            if (DateTime.TryParse(arrHoudbaarheidsdatum[i], out datum) && (DateTime.Now > datum))
                                            { Console.WriteLine($"De houdbaarheidsdatum van {arrProductNaam[i]} is verlopen. Gelieve de resterende voorraad weg te gooien en het aantal in stock aan te passen."); }

                                            else if (DateTime.TryParse(arrHoudbaarheidsdatum[i], out datum) && (DateTime.Now.AddDays(1) > datum))
                                            { Console.WriteLine($"De houdbaarheid van {arrProductNaam[i]} vervalt morgen. Gelieve de resterende stock met houdbaarheidsdatum {arrHoudbaarheidsdatum[i]} vandaag op te gebruiken en de nieuwe houdbaarheidsdatum in te voeren in de applicatie."); }

                                            else if (DateTime.TryParse(arrHoudbaarheidsdatum[i], out datum) && (DateTime.Now.AddDays(7) > datum))
                                            { Console.WriteLine($"De houdbaarheid van {arrProductNaam[i]} vervalt binnen de 7 dagen. Gelieve de resterende stock met houdbaarheidsdatum {arrHoudbaarheidsdatum[i]} zo snel mogelijk op te gebruiken en de nieuwe houdbaarheidsdatum in te voeren in de applicatie."); }

                                            else if (DateTime.TryParse(arrHoudbaarheidsdatum[i], out datum) && (DateTime.Now.AddDays(14) > datum))
                                            { Console.WriteLine($"De houdbaarheid van {arrProductNaam[i]} vervalt binnen de 14 dagen. Gelieve de resterende stock met houdbaarheidsdatum {arrHoudbaarheidsdatum[i]} zo snel mogelijk op te gebruiken en de nieuwe houdbaarheidsdatum in te voeren in de applicatie."); }

                                        }
                                        Console.WriteLine();
                                        Console.WriteLine();
                                        Console.WriteLine("Druk op enter om terug te gaan naar het hoofdmenu.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        exit6 = true;
                                        break;
                                    case 2:
                                        Console.Clear();
                                        Console.WriteLine($"{"   Naam".PadRight(33)} {"Telefoonnummer".PadRight(30)} {"Saldo".PadRight(30)} {"Stempels".PadRight(30)}");
                                        Console.WriteLine();

                                        for (int i = 0; i < arrKlantNaam.Length; i++)
                                        {
                                            if (arrKlantNaam[i] != null)
                                            { Console.WriteLine($"{i + 1}. {arrKlantNaam[i].PadRight(30)} {arrTelefoonnummer[i].PadRight(30)} {arrTotaalSaldo[i].ToString().PadRight(30)} {arrSchijvenVan10[i].ToString().PadRight(30)}"); }
                                        }
                                        Console.WriteLine();
                                        Console.WriteLine("Druk op enter om terug te gaan naar het hoofdmenu.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        exit6 = true;
                                        break;
                                    default:
                                        Console.WriteLine("Ongeldige optie. Kies een geldig nummer.");
                                        break;
                                }
                            }



                            break;
                        case 4:
                            exit7 = false;
                            exit8 = false;
                            while (!exit7)
                            {
                                Console.Clear();
                                exit7 = false;
                                individueleRekening = 0;
                                schijfVan10 = 0;
                                bestelling = "";


                                do
                                {
                                    Console.WriteLine("Wenst u de kassa te sluiten?");
                                    invoerString = Console.ReadLine();
                                } while (invoerString.ToUpper() != "JA" && invoerString.ToUpper() != "NEE");

                                if (invoerString.ToUpper() == "JA")
                                {
                                    exit7 = true; exit8 = true;
                                }

                                else
                                {
                                    do
                                    {
                                        Console.WriteLine("Staat de klant al geregistreerd?");
                                        invoerString = Console.ReadLine();
                                    } while (invoerString.ToUpper() != "JA" && invoerString.ToUpper() != "NEE");

                                    if (invoerString.ToUpper() == "NEE")
                                    {
                                        do
                                        {
                                            Console.WriteLine("Wenst de klant een klantenkaartje?");
                                            invoerString = Console.ReadLine();
                                        } while (invoerString.ToUpper() != "JA" && invoerString.ToUpper() != "NEE");

                                        if (invoerString.ToUpper() == "JA")
                                        {
                                            Console.WriteLine("Hoe heet de klant?");
                                            nieuwKlantNaam = Console.ReadLine();

                                            while (arrKlantNaam.Contains(nieuwKlantNaam, StringComparer.OrdinalIgnoreCase))
                                            {
                                                Console.WriteLine("Deze klant bestaat al.");
                                                Console.WriteLine("Voeg een klant toe:");
                                                nieuwKlantNaam = Console.ReadLine();
                                            }

                                            arrTempString1 = new string[arrKlantNaam.Length + 1];
                                            for (int i = 0; i < arrKlantNaam.Length; i++)
                                            {
                                                arrTempString1[i] = arrKlantNaam[i];
                                            }
                                            arrTempString1[arrKlantNaam.Length] = nieuwKlantNaam;
                                            arrKlantNaam = arrTempString1;

                                            arrTempString2 = new string[arrTelefoonnummer.Length + 1];
                                            for (int i = 0; i < arrTelefoonnummer.Length; i++)
                                            {
                                                arrTempString2[i] = arrTelefoonnummer[i];
                                            }
                                            VraagTelefoonnummer(nieuwKlantNaam, out nieuwTelefoonnummer);
                                            arrTempString2[arrTelefoonnummer.Length] = nieuwTelefoonnummer;
                                            arrTelefoonnummer = arrTempString2;

                                            arrTempDouble1 = new double[arrTotaalSaldo.Length + 1];
                                            for (int i = 0; i < arrTotaalSaldo.Length; i++)
                                            {
                                                arrTempDouble1[i] = arrTotaalSaldo[i];
                                            }

                                            arrTempDouble1[arrTotaalSaldo.Length] = 0;
                                            arrTotaalSaldo = arrTempDouble1;



                                            arrTempInt1 = new int[arrSchijvenVan10.Length + 1];
                                            for (int i = 0; i < arrSchijvenVan10.Length; i++)
                                            {
                                                arrTempInt1[i] = arrSchijvenVan10[i];
                                            }

                                            arrTempInt1[arrSchijvenVan10.Length] = 0;
                                            arrSchijvenVan10 = arrTempInt1;
                                        }
                                        else
                                        { nieuwKlantNaam = "ONBEKEND"; }

                                    }

                                    else
                                    {
                                        do
                                        {
                                            Console.WriteLine("Hoe heet de klant? Let op de hoofdletters.");
                                            nieuwKlantNaam = Console.ReadLine();
                                        } while (!arrKlantNaam.Contains(nieuwKlantNaam));

                                    }

                                    do
                                    {
                                        Console.Clear();
                                        exit8 = false;
                                        Console.WriteLine("Wat wenst de klant te bestellen?");
                                        Console.WriteLine("1. Friet");
                                        Console.WriteLine("2. Saus");
                                        Console.WriteLine("3. Vlees");
                                        Console.WriteLine("4. Drank");
                                        Console.WriteLine("5. Afrekenen");
                                        Console.WriteLine("6. Annuleren, vergeet de porties niet terug te nemen.");

                                        int.TryParse(Console.ReadLine(), out int keuze4);
                                        switch (keuze4)
                                        {
                                            case 1:
                                                Console.Clear();
                                                j = 1;
                                                for (int i = 0; i < arrCategorieProducten.Length; i++)
                                                {
                                                    if (arrCategorieProducten[i] == "FRIET")
                                                    {
                                                        Console.WriteLine($"{j}. {arrProductNaam[i]}");
                                                        j++;
                                                    }

                                                }
                                                do
                                                {
                                                    exit9 = false;
                                                    do
                                                    {
                                                        Console.WriteLine("Wat voor pak friet bestelt de klant? Gelieve het uit te typen zoals in de lijst staat.");
                                                        invoerString = Console.ReadLine();
                                                    } while (!arrProductNaam.Contains(invoerString, StringComparer.OrdinalIgnoreCase));
                                                    index = Array.IndexOf(arrProductNaam, invoerString);
                                                    while (index == -1)
                                                    {
                                                        Console.WriteLine("Gelieve rekening te houden met de juiste hoofdletters. Probeer nog een keer.");
                                                        invoerString = Console.ReadLine();
                                                        index = Array.IndexOf(arrProductNaam, invoerString);
                                                    }
                                                    Console.WriteLine("Welk aantal bestelt de Klant?");
                                                    int.TryParse(Console.ReadLine(), out int aantalPorties);
                                                    individueleRekening += (aantalPorties * arrVerkoopprijsProducten[index]);
                                                    aankoopprijsVandaag += (aantalPorties * arrAankoopprijsProducten[index]);
                                                    if (invoerString == "Mini")
                                                    {
                                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                                        {
                                                            if (arrCategorieProducten[i] == "FRIET")
                                                            { arrAantalStock[i] -= 100; }
                                                        }
                                                    }
                                                    else if (invoerString == "Klein")
                                                    {
                                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                                        {
                                                            if (arrCategorieProducten[i] == "FRIET")
                                                            { arrAantalStock[i] -= 150; }
                                                        }

                                                    }
                                                    else if (invoerString == "Middel")
                                                    {
                                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                                        {
                                                            if (arrCategorieProducten[i] == "FRIET")
                                                            { arrAantalStock[i] -= 225; }
                                                        }

                                                    }
                                                    else if (invoerString == "Groot")
                                                    {
                                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                                        {
                                                            if (arrCategorieProducten[i] == "FRIET")
                                                            { arrAantalStock[i] -= 300; }
                                                        }

                                                    }
                                                    else if (invoerString == "Familiepak")
                                                    {
                                                        for (int i = 0; i < arrProductNaam.Length; i++)
                                                        {
                                                            if (arrCategorieProducten[i] == "FRIET")
                                                            { arrAantalStock[i] -= 700; }
                                                        }

                                                    }


                                                    bestelling += aantalPorties + " keer " + arrProductNaam[index] + " voor " + (arrVerkoopprijsProducten[index] * aantalPorties) + " euro." + Environment.NewLine;
                                                    exit9 = true;

                                                } while (!exit9);
                                                break;
                                            case 2:
                                                Console.Clear();
                                                j = 1;
                                                for (int i = 0; i < arrCategorieProducten.Length; i++)
                                                {
                                                    if (arrCategorieProducten[i] == "SAUS")
                                                    {
                                                        Console.WriteLine($"{j}. {arrProductNaam[i]}");
                                                        j++;
                                                    }

                                                }
                                                do
                                                {
                                                    exit9 = false;
                                                    do
                                                    {
                                                        Console.WriteLine("Wat voor saus bestelt de klant? Gelieve het uit te typen zoals in de lijst staat.");
                                                        invoerString = Console.ReadLine();
                                                    } while (!arrProductNaam.Contains(invoerString, StringComparer.OrdinalIgnoreCase));
                                                    index = Array.IndexOf(arrProductNaam, invoerString);
                                                    while (index == -1)
                                                    {
                                                        Console.WriteLine("Gelieve rekening te houden met de juiste hoofdletters. Probeer nog een keer.");
                                                        invoerString = Console.ReadLine();
                                                        index = Array.IndexOf(arrProductNaam, invoerString);
                                                    }
                                                    Console.WriteLine("Welk aantal bestelt de Klant?");
                                                    int.TryParse(Console.ReadLine(), out int aantalPorties);
                                                    individueleRekening += (aantalPorties * arrVerkoopprijsProducten[index]);
                                                    aankoopprijsVandaag += (aantalPorties * arrAankoopprijsProducten[index]);
                                                    arrAantalStock[index] -= (aantalPorties * 50);
                                                    bestelling += aantalPorties + " keer " + arrProductNaam[index] + " voor " + (arrVerkoopprijsProducten[index] * aantalPorties) + " euro." + Environment.NewLine;
                                                    exit9 = true;

                                                } while (!exit9);
                                                break;
                                            case 3:
                                                Console.Clear();
                                                j = 1;
                                                for (int i = 0; i < arrCategorieProducten.Length; i++)
                                                {
                                                    if (arrCategorieProducten[i] == "VLEES")
                                                    {
                                                        Console.WriteLine($"{j}. {arrProductNaam[i]}");
                                                        j++;
                                                    }

                                                }
                                                do
                                                {
                                                    exit9 = false;
                                                    do
                                                    {
                                                        Console.WriteLine("Wat voor vlees bestelt de klant? Gelieve het uit te typen zoals in de lijst staat.");
                                                        invoerString = Console.ReadLine();
                                                    } while (!arrProductNaam.Contains(invoerString, StringComparer.OrdinalIgnoreCase));
                                                    index = Array.IndexOf(arrProductNaam, invoerString);
                                                    while (index == -1)
                                                    {
                                                        Console.WriteLine("Gelieve rekening te houden met de juiste hoofdletters. Probeer nog een keer.");
                                                        invoerString = Console.ReadLine();
                                                        index = Array.IndexOf(arrProductNaam, invoerString);
                                                    }
                                                    Console.WriteLine("Welk aantal bestelt de Klant?");
                                                    int.TryParse(Console.ReadLine(), out int aantalPorties);
                                                    individueleRekening += (aantalPorties * arrVerkoopprijsProducten[index]);
                                                    aankoopprijsVandaag += (aantalPorties * arrAankoopprijsProducten[index]);
                                                    arrAantalStock[index] -= aantalPorties;
                                                    bestelling += aantalPorties + " keer " + arrProductNaam[index] + " voor " + (arrVerkoopprijsProducten[index] * aantalPorties) + " euro." + Environment.NewLine;
                                                    exit9 = true;

                                                } while (!exit9);
                                                break;
                                            case 4:
                                                Console.Clear();
                                                j = 1;
                                                for (int i = 0; i < arrCategorieProducten.Length; i++)
                                                {
                                                    if (arrCategorieProducten[i] == "DRANK")
                                                    {
                                                        Console.WriteLine($"{j}. {arrProductNaam[i]}");
                                                        j++;
                                                    }

                                                }
                                                do
                                                {
                                                    exit9 = false;
                                                    do
                                                    {
                                                        Console.WriteLine("Wat voor drank bestelt de klant? Gelieve het uit te typen zoals in de lijst staat.");
                                                        invoerString = Console.ReadLine();
                                                    } while (!arrProductNaam.Contains(invoerString, StringComparer.OrdinalIgnoreCase));
                                                    index = Array.IndexOf(arrProductNaam, invoerString);
                                                    while (index == -1)
                                                    {
                                                        Console.WriteLine("Gelieve rekening te houden met de juiste hoofdletters. Probeer nog een keer.");
                                                        invoerString = Console.ReadLine();
                                                        index = Array.IndexOf(arrProductNaam, invoerString);
                                                    }
                                                    Console.WriteLine("Welk aantal bestelt de Klant?");
                                                    int.TryParse(Console.ReadLine(), out int aantalPorties);
                                                    individueleRekening += (aantalPorties * arrVerkoopprijsProducten[index]);
                                                    aankoopprijsVandaag += (aantalPorties * arrAankoopprijsProducten[index]);
                                                    arrAantalStock[index] -= aantalPorties;
                                                    bestelling += aantalPorties + " keer " + arrProductNaam[index] + " voor " + (arrVerkoopprijsProducten[index] * aantalPorties) + " euro." + Environment.NewLine;
                                                    exit9 = true;

                                                } while (!exit9);
                                                break;
                                            case 5:
                                                Console.WriteLine(bestelling);
                                                Console.WriteLine($"Afrekening : {individueleRekening} euro.");
                                                do
                                                {
                                                    Console.WriteLine("Klopt deze rekening?");
                                                    invoerString = Console.ReadLine();
                                                } while (invoerString.ToUpper() != "JA" && invoerString.ToUpper() != "NEE");
                                                if (invoerString.ToUpper() == "JA")
                                                {
                                                    if (nieuwKlantNaam != "ONBEKEND")
                                                    {
                                                        index = Array.IndexOf(arrKlantNaam, nieuwKlantNaam);
                                                        arrTotaalSaldo[index] += individueleRekening;
                                                        Console.WriteLine($"Het nieuwe totaalsaldo van {nieuwKlantNaam} is {arrTotaalSaldo[index]}.");
                                                        schijfVan10 = (int)Math.Floor((double)individueleRekening / 10);
                                                        arrSchijvenVan10[index] += schijfVan10;
                                                        if (arrSchijvenVan10[index] >= 10)
                                                        {
                                                            arrSchijvenVan10[index] = 0;
                                                            Console.WriteLine($"De stempelkaart van {nieuwKlantNaam} is vol. Hij krijgt 5 euro korting.");
                                                            Console.WriteLine($"De nieuwe afrekening bedraagt {individueleRekening - 5} euro.");
                                                            omzetVandaag -= 5;
                                                        }
                                                    }

                                                    omzetVandaag += individueleRekening;
                                                    Console.WriteLine($"De omzet van vandaag bedraagt {omzetVandaag} euro, wat goed is voor een winst van {(omzetVandaag - aankoopprijsVandaag).ToString("0.00")} euro op producten.");
                                                    Console.ReadKey();
                                                    exit8 = true;


                                                }





                                                break;
                                            case 6:
                                                exit8 = true;

                                                break;
                                            default:
                                                Console.WriteLine("Ongeldige optie. Kies een geldig nummer.");
                                                break;
                                        }
                                    } while (!exit8);
                                }
                            }
                            break;
                        case 5:
                            Console.Clear();
                            for (int i = 0; i < arrProductNaam.Length; i++)
                            {
                                alleProducten1 += arrProductNaam[i] + ";" + arrCategorieProducten[i] + ";" + arrVerkoopprijsProducten[i] + ";" + arrAankoopprijsProducten[i] + ";" + arrAantalStock[i] + ";" + arrMinimumVoorraadNiveau[i] + ";" + arrHoudbaarheidsdatum[i] + ";" + Environment.NewLine;
                            }
                            for (int i = 0; i < arrKlantNaam.Length; i++)
                            {
                                alleKlanten1 += arrKlantNaam[i] + ";" + arrTelefoonnummer[i] + ";" + arrTotaalSaldo[i] + ";" + arrSchijvenVan10[i] + ";" + Environment.NewLine;
                            }

                            do
                            {
                                Console.WriteLine("Wil je de gegevens opslaan? Ja/Nee");
                                invoerString = Console.ReadLine();
                            } while (invoerString.ToUpper() != "JA" && invoerString.ToUpper() != "NEE");

                            if (invoerString.ToUpper() == "JA")
                            {
                                using (StreamWriter writer = new StreamWriter("productgegevens.txt"))
                                {
                                    writer.WriteLine(alleProducten1);
                                    Console.WriteLine("Uw gegevens worden weggeschreven naar productgegevens.txt.");
                                }
                                using (StreamWriter writer = new StreamWriter("klantgegevens.txt"))
                                {
                                    writer.WriteLine(alleKlanten1);
                                    Console.WriteLine("Uw gegevens worden weggeschreven naar klantgegevens.txt.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Uw gegevens worden NIET weggeschreven.");
                            }



                            Console.WriteLine("Het programma wordt afgesloten.");
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Ongeldige optie. Kies een geldig nummer.");
                            break;
                    }
                }
            }

            Console.WriteLine();


            static bool IsGeldigePrijs(string invoer)
            {
                double prijs;
                if (double.TryParse(invoer, out prijs) && prijs >= 0)
                {
                    return true;
                }
                else { return false; }
            }

            static bool IsGeldigGetal(string invoer)
            {
                int getal;
                if (int.TryParse(invoer, out getal) && getal >= 0)
                {
                    return true;
                }
                else { return false; }
            }

            static bool IsGeldigeDatum(string invoer)
            {
                if (DateTime.TryParseExact(invoer, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out DateTime result))
                {
                    string parsedDatum = result.ToString("dd/MM/yyyy");
                    if (parsedDatum == invoer)
                    {
                        return true;
                    }
                    else { return false; };
                }
                else { return false; }
            }
            static void VraagCategorie(string nieuwProduct, out string nieuwCategorie)
            {
                Console.WriteLine($"Onder welke categorie valt een {nieuwProduct}? Friet, Vlees, Saus of Drank?");
                nieuwCategorie = Console.ReadLine().ToUpper();
                while (nieuwCategorie != "FRIET" && nieuwCategorie != "VLEES" && nieuwCategorie != "SAUS" && nieuwCategorie != "DRANK")
                {
                    Console.WriteLine("Ongeldige invoer. Voer Friet, Vlees, Saus of Drank in.");
                    nieuwCategorie = Console.ReadLine().ToUpper();
                }
            }
            static void VraagTelefoonnummer(string nieuwKlantNaam, out string nieuwTelefoonnummer)
            {
                Console.WriteLine($"Wat is het telefoonnummer van {nieuwKlantNaam}?");
                nieuwTelefoonnummer = Console.ReadLine();
                while (nieuwTelefoonnummer.Length != 10 && nieuwTelefoonnummer.All(char.IsDigit))
                {
                    Console.WriteLine("Ongeldige invoer. Een telefoonnummer telt 10 cijfers. Probeer opnieuw.");
                    nieuwTelefoonnummer = Console.ReadLine();
                }
            }
            static void VraagVerkoopprijs(string nieuwProduct, out string nieuwVerkoopprijs)
            {
                Console.WriteLine($"Tegen welke prijs wil je een portie {nieuwProduct} verkopen?");
                nieuwVerkoopprijs = Console.ReadLine();
                while (!IsGeldigePrijs(nieuwVerkoopprijs))
                {
                    Console.WriteLine("Dit is geen geldige prijs, gelieve een in te voeren.");
                    nieuwVerkoopprijs = Console.ReadLine();
                }
            }
            static void VraagAankoopprijs(string nieuwProduct, out string nieuwAankoopprijs)
            {
                Console.WriteLine($"Tegen welke prijs heb je je {nieuwProduct} per stuk aangekocht?");
                nieuwAankoopprijs = Console.ReadLine();
                while (!IsGeldigePrijs(nieuwAankoopprijs))
                {
                    Console.WriteLine("Dit is geen geldige prijs, gelieve een in te voeren");
                    nieuwAankoopprijs = Console.ReadLine();
                }
            }
            static void VraagTotaalSaldo(string nieuwKlantNaam, out string nieuwTotaalSaldo)
            {
                Console.WriteLine($"Hoeveel heeft {nieuwKlantNaam} al uitgegeven in de frituur?");
                nieuwTotaalSaldo = Console.ReadLine();
                while (!IsGeldigePrijs(nieuwTotaalSaldo))
                {
                    Console.WriteLine("Dit is geen geldige prijs, gelieve er een in te voeren.");
                    nieuwTotaalSaldo = Console.ReadLine();
                }
            }
            static void VraagAantalStock(string nieuwProduct, out string nieuwAantalStock)
            {
                Console.WriteLine($"Hoeveel porties {nieuwProduct} heb je in stock?");
                nieuwAantalStock = Console.ReadLine();
                while (!IsGeldigGetal(nieuwAantalStock))
                {
                    Console.WriteLine("Dit is geen geldig aantal, gelieve een in te voeren");
                    nieuwAantalStock = Console.ReadLine();
                }
            }
            static void VraagAantalSchijven(string nieuwKlantNaam, out string nieuwSchijvenVan10)
            {
                Console.WriteLine($"Hoeveel stempels heeft {nieuwKlantNaam} op zijn stempelkaart?");
                nieuwSchijvenVan10 = Console.ReadLine();
                while (!IsGeldigGetal(nieuwSchijvenVan10) && Convert.ToInt32(nieuwSchijvenVan10) < 10)
                {
                    Console.WriteLine("Dit is geen geldig aantal, gelieve er een in te voeren. Het aantal stempels dat een klant heeft, kan nooit meer dan 9 zijn.");
                    nieuwSchijvenVan10 = Console.ReadLine();
                }
            }
            static void VraagMinimumVoorraadNiveau(string nieuwProduct, out string nieuwMinimumVoorraadNiveau)
            {
                Console.WriteLine($"Bij welk aantal in stock moet er een nieuwe batch {nieuwProduct} besteld worden?");
                nieuwMinimumVoorraadNiveau = Console.ReadLine();
                while (!IsGeldigGetal(nieuwMinimumVoorraadNiveau))
                {
                    Console.WriteLine("Dit is geen geldig aantal, gelieve een in te voeren");
                    nieuwMinimumVoorraadNiveau = Console.ReadLine();
                }
            }
            static void VraagHoudbaarheidsdatum(string nieuwProduct, out string nieuwHoudbaarheidsdatum)
            {
                Console.WriteLine($"Welke houdbaarheidsdatum heeft een {nieuwProduct}? Geef een datum in in het volgende formaat dd/mm/yyyy.");
                nieuwHoudbaarheidsdatum = Console.ReadLine();
                while (!IsGeldigeDatum(nieuwHoudbaarheidsdatum))
                {
                    Console.WriteLine("Dit is geen geldige houdbaarheidsdatum, gelieve de datum in het volgende format in te voeren : dd/mm/yyyy.");
                    nieuwHoudbaarheidsdatum = Console.ReadLine();
                }
            }

        }
    }
}








